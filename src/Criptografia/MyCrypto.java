package Criptografia;

import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MyCrypto
{
    static String plainText = "teste";
    public static void main(String[] args) throws Exception{
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        SecretKey key = keyGenerator.generateKey();
        byte[] IV = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        System.out.println("Original Text  : "+plainText);
        
        byte[] cipherText = encrypt(plainText.getBytes(),key, IV);
        String textoCriptografado = Base64.getEncoder().encodeToString(cipherText);
        
        System.out.println("Encrypted Text : "+textoCriptografado);
        byte[] tt = Base64.getDecoder().decode(textoCriptografado);
        String decryptedText = decrypt(cipherText,key, IV);
        String t = decrypt(tt,key, IV);
        System.out.println("iv:"+IV);
        System.out.println("key:"+key.getEncoded());
        System.out.println("cript:"+cipherText);
        System.out.println("DeCrypted Text : "+decryptedText);
        System.out.println("DeCrypted Text : "+t);
        
    }
    
    public static byte[] encrypt (byte[] plaintext,SecretKey key,byte[] IV ) throws Exception{
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        byte[] cipherText = cipher.doFinal(plaintext);
        return cipherText;
    }
    
    public static String decrypt (byte[] cipherText, SecretKey key,byte[] IV) throws Exception{
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        byte[] decryptedText = cipher.doFinal(cipherText);
        return new String(decryptedText);
    }
}