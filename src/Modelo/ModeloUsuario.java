package Modelo;

public class ModeloUsuario {
	private  int id;
	private  String nome;
	private  String senhaCriptografado;
	private  String emailCriptografado;
	private  String iv;
	private  String keey;
	
	public String getIv() {
		return iv;
	}
	public void setIv(String iv) {
		this.iv = iv;
	}
	public String getKeey() {
		return keey;
	}
	public void setKeey(String keey) {
		this.keey = keey;
	}
	public int getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSenhaCriptografado() {
		return senhaCriptografado;
	}
	public void setSenhaCriptografado(String senhaCriptografado) {
		this.senhaCriptografado = senhaCriptografado;
	}
	public String getEmailCriptografado() {
		return emailCriptografado;
	}
	public void setEmailCriptografado(String emailCriptografado) {
		this.emailCriptografado = emailCriptografado;
	}		
	}
