package Modelo;

public class ModeloHome {
   private int id;
   private String textoEsquerdo;
   private String textoDireito;
   private Long img;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getTextoEsquerdo() {
			return textoEsquerdo;
		}
		public void setTextoEsquerdo(String textoEsquerdo) {
			this.textoEsquerdo = textoEsquerdo;
		}
		public String getTextoDireito() {
			return textoDireito;
		}
		public void setTextoDireito(String textoDireito) {
			this.textoDireito = textoDireito;
		}
		public Long getImg() {
			return img;
		}
		public void setImg(Long img) {
			this.img = img;
		}
   }
