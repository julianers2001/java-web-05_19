package Modelo;

public class ModeloCuriosidades {
	private int id;
	private String texto;
	private Long img;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Long getImg() {
		return img;
	}
	public void setImg(Long img) {
		this.img = img;
	}

}
