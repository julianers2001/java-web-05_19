package Beans;

//import java.io.InputStream;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import Controle.ControleHome;
import Modelo.ModeloHome;

@ManagedBean(name="homeBean")
@RequestScoped

public class Home {
	   private String textoEsquerdo;
	   private String textoDireito;
	   private int id;
	//   private Long img;
	   
	   ArrayList<ModeloHome> lista = new ControleHome().consultarHome();
		public ArrayList<ModeloHome> getLista() {
			return lista;
		}
		public void setLista(ArrayList<ModeloHome> lista) {
			this.lista = lista;
		}
		public ModeloHome retornarItem(int id) {
			return lista.get(id);
		}
	   public boolean enviar() {
		   boolean resultado = false;
			try {
				ControleHome homec = new ControleHome();				
				ModeloHome homem = new ModeloHome();
		//	    InputStream inputStream = null;
			//    Part filePart = request.getPart(img);
				homem.setTextoDireito(textoDireito);
				homem.setTextoEsquerdo(textoEsquerdo);
			  //  homem.setImg(inputStream);
				resultado = homec.inserir(homem);											
			}catch(Exception e) {
				e.getMessage();
			}
     	   return resultado;
	   }
		public boolean excluir() {
			 boolean resultado = false;
				try {
			ControleHome homec = new ControleHome();				
			int id = 1;
			resultado = homec.excluir(id);											
		}catch(Exception e) {
			e.getMessage();
		}
		
			return resultado;
		}
		public boolean editar() {
			 boolean resultado = false;
				try {
					ControleHome homec = new ControleHome();				
					ModeloHome homem = new ModeloHome();
					int id = 1;
					homem.setId(id);
					homem.setTextoDireito(textoDireito);
					homem.setTextoEsquerdo(textoEsquerdo);
					resultado = homec.editar(homem);											
				}catch(Exception e) {
					e.getMessage();
				}
	     	   return resultado;
		}
		public String getTextoEsquerdo() {
			return textoEsquerdo;
		}
		public void setTextoEsquerdo(String textoEsquerdo) {
			this.textoEsquerdo = textoEsquerdo;
		}
		public String getTextoDireito() {
			return textoDireito;
		}
		public void setTextoDireito(String textoDireito) {
			this.textoDireito = textoDireito;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
}
