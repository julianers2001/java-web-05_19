package Beans;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import Controle.ControleUsuario;
import Criptografia.MyCrypto;
import Modelo.ModeloUsuario;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="userBean")
@RequestScoped
@SessionScoped

public class User {
	private  String senhaCriptografado2;
	private  String emailCriptografado2;
	ArrayList<ModeloUsuario> lista = new ControleUsuario().consultarUsuario();
	
	public ArrayList<ModeloUsuario> getLista() {
		return lista;
	}
	
	public boolean contem() {
		boolean feito = false;
		ArrayList<ModeloUsuario> lista = new ControleUsuario().consultarUsuario();
		if(lista.size() > 0) {
			feito = true;
		}
		return feito;
	}
	public void setLista(ArrayList<ModeloUsuario> lista) {
		this.lista = lista;
	}
	public ModeloUsuario retornarItem(int id) {
		return lista.get(id);
	}
	public boolean login(){		
		boolean resultado = false;
		ModeloUsuario model = new ModeloUsuario();
		ControleUsuario control = new ControleUsuario();
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(256);
        SecretKey key = keyGenerator.generateKey();
        byte[] IV = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        byte[] emailCripto = MyCrypto.encrypt(emailCriptografado2.getBytes(),key, IV);
        String cpE = Base64.getEncoder().encodeToString(emailCripto);
        byte[] senhaCripto = MyCrypto.encrypt(senhaCriptografado2.getBytes(),key, IV);
        String cpS = Base64.getEncoder().encodeToString(senhaCripto);
        String iv = Base64.getEncoder().encodeToString(IV);
        String kee = Base64.getEncoder().encodeToString(key.getEncoded());
        model.setEmailCriptografado(cpE);
        model.setSenhaCriptografado(cpS);
        model.setIv(iv);
        model.setKeey(kee);	
        control.inserir(model); 	
		resultado = true;
	}catch(Exception e) {
		e.getMessage();
	}
	return resultado;
	
}
public boolean verificar() {
	boolean resultado = false;
			ModeloUsuario crip = new ModeloUsuario();
			ControleUsuario control = new ControleUsuario();
			try {	
				crip = control.consultaID(1);
		        byte[] tt = Base64.getDecoder().decode(crip.getEmailCriptografado());
		        byte[] t1 = Base64.getDecoder().decode(crip.getSenhaCriptografado());
		        byte[] key = Base64.getDecoder().decode(crip.getKeey());
		        byte[] iv = Base64.getDecoder().decode(crip.getIv());
		        SecretKey originalKey = new SecretKeySpec(key, 0, key.length, "AES");
		        System.out.println("teste"+tt+"-"+t1);
				String emailD = MyCrypto.decrypt(tt,originalKey, iv);
		        String senhaD = MyCrypto.decrypt(t1,originalKey, iv);
		        System.out.println("Email : "+emailD+"-"+emailCriptografado2);
		        System.out.println("Senha : "+senhaD+"-"+senhaCriptografado2);
		        if(emailCriptografado2.equals(emailD) && senhaCriptografado2.equals(senhaD)) {
		        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("teste", emailD);
		        System.out.println("Sess�o>>>"+FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("teste")); 
		        }else {
		        	System.out.println("N�o deu bom");
		        }
				resultado = true;
			} catch (Exception e) {
				e.printStackTrace();
			}				
	return resultado;
}
public String getSenhaCriptografado2() {
	return senhaCriptografado2;
}
public void setSenhaCriptografado2(String senhaCriptografado2) {
	this.senhaCriptografado2 = senhaCriptografado2;
}
public String getEmailCriptografado2() {
	return emailCriptografado2;
}
public void setEmailCriptografado2(String emailCriptografado2) {
	this.emailCriptografado2 = emailCriptografado2;
}
}
