package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Modelo.ModeloComentario;

public class ControleComentario {
	public ArrayList<ModeloComentario> consulta() throws SQLException{
		ArrayList<ModeloComentario> a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM comentario;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ArrayList<ModeloComentario>();
				while(rs.next()) {
					ModeloComentario l = new ModeloComentario();
					l.setId(rs.getInt("id"));
					l.setTexto(rs.getString("texto"));
					l.setNome(rs.getString("nome"));
					l.setTitulo(rs.getString("titulo"));
					a.add(l);
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao inserir.");

			}
		return a;
	}

	public boolean inserir(ModeloComentario Comentario) throws SQLException{
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO comentario(texto,titulo,nome) VALUES(?,?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, Comentario.getTexto());
			ps.setString(2, Comentario.getTitulo());
			ps.setString(3, Comentario.getNome());
			if(!ps.execute()) {
				resultado = true;		
				new Conexao().fecharConexao(con);		
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	
	public ModeloComentario consultaID(int n) throws SQLException{
		ModeloComentario a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM comentario where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ModeloComentario();
				while(rs.next()) {
					a.setId(rs.getInt("id"));
					a.setNome(rs.getString("nome"));
					a.setTexto(rs.getString("texto"));
					a.setTitulo(rs.getString("titulo"));
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return a;
	}

}
