package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Modelo.ModeloEspecies;

public class ControleEspecies {
	public ArrayList<ModeloEspecies> consulta() throws SQLException{
		ArrayList<ModeloEspecies> a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM Especies;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ArrayList<ModeloEspecies>();
				while(rs.next()) {
					ModeloEspecies l = new ModeloEspecies();
					l.setId(rs.getInt("id"));
					l.setTexto(rs.getString("texto"));
					l.setNomeDaEspecie(rs.getString("nomeDaEspecie"));
					l.setImg(rs.getLong("img"));
					a.add(l);
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao inserir.");

			}
		return a;
	}

	public boolean inserir(ModeloEspecies especies) throws SQLException{
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO especies(texto,img,nomeDaEspecie) VALUES(?,?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, especies.getTexto());
			ps.setLong(2, especies.getImg());
			ps.setString(3, especies.getNomeDaEspecie());
			if(!ps.execute()) {
				resultado = true;		
				new Conexao().fecharConexao(con);		
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	
	public ModeloEspecies consultaID(int n) throws SQLException{
		ModeloEspecies a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM especies where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ModeloEspecies();
				while(rs.next()) {
					a.setId(rs.getInt("id"));
					a.setNomeDaEspecie(rs.getString("nomeDaEspecie"));
					a.setTexto(rs.getString("texto"));
					a.setImg(rs.getLong("img"));
		
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return a;
	}
}
