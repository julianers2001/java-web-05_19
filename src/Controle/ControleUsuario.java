package Controle;
	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;
	import Modelo.ModeloUsuario;

	public class ControleUsuario {
		public ArrayList<ModeloUsuario> consultarUsuario(){
			ArrayList<ModeloUsuario> lista = null;
			try {
				Connection con = new Conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM usuario;");
				ResultSet rs = ps.executeQuery();
				if(rs != null) {
					lista = new ArrayList<ModeloUsuario>();
					while(rs.next()) {
						ModeloUsuario user = new ModeloUsuario();
						user.setId(rs.getInt("id"));
						user.setEmailCriptografado(rs.getString("emailCriptografado"));
						user.setSenhaCriptografado(rs.getString("senhaCriptografado"));
						user.setIv(rs.getString("iv"));
						user.setKeey(rs.getString("keey"));
						lista.add(user);
					}
				}
				new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;
		}
		
		public boolean inserir(ModeloUsuario cripto) throws SQLException{
			boolean resultado = false;	
			try {
				Connection con = new Conexao().abrirConexao();
				String sql = "INSERT INTO usuario(emailCriptografado,senhaCriptografado,iv,keey) VALUES(?,?,?,?);";	
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, cripto.getEmailCriptografado());
				ps.setString(2, cripto.getSenhaCriptografado());
				ps.setString(3, cripto.getIv());
				ps.setString(4, cripto.getKeey());
				if(!ps.execute()) {
					resultado = true;		
					new Conexao().fecharConexao(con);		
				}
			}catch(SQLException e) {
				System.out.println(e.getMessage());	
			}
			return resultado;	
		}
		
		public ModeloUsuario consultaID(int n) throws SQLException{
			ModeloUsuario a = null;
				Connection conn = new Conexao().abrirConexao();
				String sql = "SELECT * FROM usuario where id= ?;";
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setInt(1, n);
				ResultSet rs = ps.executeQuery();		
				if(rs != null) {
					a = new ModeloUsuario();
					while(rs.next()) {
						a.setId(rs.getInt("id"));
						a.setEmailCriptografado(rs.getString("emailCriptografado"));
						a.setSenhaCriptografado(rs.getString("senhaCriptografado"));
						a.setIv(rs.getString("iv"));
						a.setKeey(rs.getString("keey"));
					}
					new Conexao().fecharConexao(conn);
				}else {
					throw new SQLException("Erro ao Consultar.");
				}
			return a;
		}
	}
