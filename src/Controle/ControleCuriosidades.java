package Controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Modelo.ModeloCuriosidades;

public class ControleCuriosidades {
	public ArrayList<ModeloCuriosidades> consulta() throws SQLException{
		ArrayList<ModeloCuriosidades> a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM curiosidades;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ArrayList<ModeloCuriosidades>();
				while(rs.next()) {
					ModeloCuriosidades l = new ModeloCuriosidades();
					l.setId(rs.getInt("id"));
					l.setTexto(rs.getString("texto"));
					l.setImg(rs.getLong("img"));
					a.add(l);
				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao inserir.");

			}
		return a;
	}

	public boolean inserir(ModeloCuriosidades curiosidades) throws SQLException{
		boolean resultado = false;	
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "INSERT INTO curiosidades(texto,img) VALUES(?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, curiosidades.getTexto());
			ps.setLong(2, curiosidades.getImg());
			if(!ps.execute()) {
				resultado = true;		
				new Conexao().fecharConexao(con);		
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());	
		}
		return resultado;	
	}
	
	public ModeloCuriosidades consultaID(int n) throws SQLException{
		ModeloCuriosidades a = null;
			Connection conn = new Conexao().abrirConexao();
			String sql = "SELECT * FROM curiosidades where id= ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, n);
			ResultSet rs = ps.executeQuery();		
			if(rs != null) {
				a = new ModeloCuriosidades();
				while(rs.next()) {
					a.setId(rs.getInt("id"));
					a.setTexto(rs.getString("texto"));
					a.setImg(rs.getLong("img"));

				}
				new Conexao().fecharConexao(conn);
			}else {
				throw new SQLException("Erro ao Consultar.");
			}
		return a;
	}
}
