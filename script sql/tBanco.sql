CREATE SCHEMA if not exists tbanco;
CREATE TABLE if not exists tbanco.usuario( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,	
	nome VARCHAR(100) NOT NULL,
	emailCriptografado VARCHAR(100) NOT NULL,
	senhaCriptografado VARCHAR(100) NOT NULL,
	iv VARCHAR(100) NOT NULL,
	keey VARCHAR(100) NOT NULL
);
CREATE TABLE if not exists tbanco.home( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,	
	textoDireito VARCHAR(1000) NOT NULL,
	textoEsquerdo VARCHAR(1000) NOT NULL,
	img Longblob NOT NULL
);
CREATE TABLE if not exists tbanco.curiosidades( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,	
	texto VARCHAR(1000) NOT NULL,
	img Longblob NOT NULL
);
CREATE TABLE if not exists tbanco.especie( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,	
	texto VARCHAR(1000) NOT NULL,
	nomeDaEspecie VARCHAR(50) NOT NULL,
	img Longblob NOT NULL
);
CREATE TABLE if not exists tbanco.comentarios( 
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,	
	nome VARCHAR(50) NOT NULL,
	titulo VARCHAR(50),
	texto VARCHAR(1000) NOT NULL
);